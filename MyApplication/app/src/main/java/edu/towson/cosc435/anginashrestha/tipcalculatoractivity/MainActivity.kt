package edu.towson.cosc435.anginashrestha.tipcalculatoractivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        calculate_btn.setOnClickListener { handleClick() }
    }

    private fun handleClick(){
        try {
            val amount = Amount.editableText.toString()

            val tipAmount = when(radioGroup.checkedRadioButtonId){
                R.id. first_radio_btn -> TipAmount.One
                R.id. second_radio_btn -> TipAmount.Two
                R.id. third_radio_btn -> TipAmount.Three
                else -> throw Exception()
            }

            val amountnum = amount.toDouble()
            val result = tip(amountnum,tipAmount)
            val total = amountnum + result
            result_tv.text = "Your calculated tip is $" + result.toString() + " and your total is $" + total.toString()
        }catch (e: Exception){
            result_tv.text ="Error"
        }

    }
}

