package edu.towson.cosc435.anginashrestha.tipcalculatoractivity

enum class TipAmount{ One, Two, Three}

fun tip(input: Double, tipAmount: TipAmount): Double {

    return when(tipAmount){
        TipAmount.One -> (input * 0.1)
        TipAmount.Two -> (input * 0.2)
        TipAmount.Three -> (input * 0.3)
    }

}